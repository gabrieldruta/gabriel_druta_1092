
const FIRST_NAME = "Gabriel";
const LAST_NAME = "Druta";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache = {};
    var emptyObj = {};
    cache.pageAccessCounter = function (param) {
        //if the param is null
        if (param == null) {
            if (emptyObj.hasOwnProperty('home')==false)
                     emptyObj['home'] = 1; //init the property 
            else
                      emptyObj['home'] = emptyObj['home'] + 1; //increment home    
         } 
        else {
            var pageName = String(param).toLowerCase();
            if (emptyObj.hasOwnProperty(pageName)==false)
                 emptyObj[pageName] = 1;
            else
                emptyObj[pageName] = emptyObj[pageName] + 1;
          
        }
    };

    cache.getCache = function () {
        return emptyObj;
    };
    
    return cache;
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

